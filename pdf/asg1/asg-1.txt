1. The two programs badLoc.c and goodLoc.c differ only in their memory access patterns over a 2D array. Because goodLoc.c reads values across an entire row before moving to the next row, it is able to take advantage of cache misses more effectively. Each sequential item is directly next to the other item in memory, so when a cache miss occurs, another chunk gets loaded into cache memory. By contrast, badLoc.c accesses elements across an entire column before moving to the next row. Each cache miss brings in data that is right next to the item being accessed, but that data is not used before being blown out of the cache by reads further down the column.

goodLoc.c runs in about 4s on my machine
badLoc.c runs in about 1s on my machine

2. 
Padding | Threads | Execution Time
--------+---------+--------------------
2       | 2       | 1.263028e+01 sec
--------+---------+--------------------
2       | 4       | 3.218884e+01 sec
--------+---------+--------------------
2       | 8       | 4.758270e+01 sec
--------+---------+--------------------
8       | 2       | 5.864917e+00 sec
--------+---------+--------------------
8       | 4       | 1.171303e+01 sec
--------+---------+--------------------
8       | 8       | 1.673807e+01 sec
--------+---------+--------------------
32      | 2       | 7.771695e+00 sec
--------+---------+--------------------
32      | 4       | 1.011865e+01 sec
--------+---------+--------------------
32      | 8       | 1.025735e+01 sec
--------+---------+--------------------
64      | 2       | 7.637217e+00 sec
--------+---------+--------------------
64      | 4       | 9.516494e+00 sec
--------+---------+--------------------
64      | 8       | 1.088294e+01 sec
--------+---------+--------------------
128     | 2       | 8.516638e+00 sec
--------+---------+--------------------
128     | 4       | 9.573303e+00 sec
--------+---------+--------------------
128     | 8       | 1.095085e+01 sec
--------+---------+--------------------
256     | 2       | 7.921379e+00 sec
--------+---------+--------------------
256     | 4       | 1.014202e+01 sec
--------+---------+--------------------
256     | 8       | 1.133848e+01 sec
--------+---------+--------------------
512     | 2       | 5.978763e+00 sec
--------+---------+--------------------
512     | 4       | 1.000146e+01
--------+---------+--------------------
512     | 8       | segfault

First, to address the segfault, any (padding, threads) combo where padding * threads > 1024 would cause this program to read past the bounds of A. It took a padding of 512 and 8 threads to produce a segfault because that was the only instance of the program that strayed far enough from A's memory address to discover memory it was not supposed to read.

Second, the program always runs slower with more threads, but faster with larger padding. The threads in this program make a large number of accesses at the same location. When more threads are introduced to the program, there is more overhead for the system to manage them. Additionally, when the padding is small, all threads are trying to write to the same cache line.




