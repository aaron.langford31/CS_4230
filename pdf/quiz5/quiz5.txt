1. Can't find this anywhere.

2a. Blocking Message Passing Routines

The send routine will block, or return after it is safe to modify application buffer. This doesn't imply that the data was received, it just means that nothing weird will happen if you change the data in your application buffer.

2b. Non-blocking Message Passing Routines

The send routine will return immediately, simply telling the MPI runtime that a message is ready to be sent. It is not safe to modify the data in the application buffer until it can be made sure that the data is safe to modify.


