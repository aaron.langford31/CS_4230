// Ganesh, 9/28/14
//
// Wrote this test to illustrate the use of Java volatiles. A successful
// test outcome (what drives the idea of volatiles home) is when you don't
// declare req and ack as volatiles; the code hangs after a few iterations.
// By declaring these variables volatile, no such hang occurs.
//
// Data races on A, req, and ack are also eliminated by such a declaration.
//
// Never managed to notice a dip in A's value; however, I suppose if one turns
// the A update into an A[i] (array) update, one might catch stale A's? Worth 
// trying.  However, that should again not happen if req/ack are volatiles.
//
// VGood : All good, no hangs, with yield and volatiles also
// 
public class VGood extends Thread{
    int tid;
    VGood(int i) {
  tid = i;
    }
    
    static int N = 100;

    // The following two volatile declarations are necessary
    // to run without hangs, and to avoid data races.
    static volatile boolean req = false;
    static volatile boolean ack = false;

    
    // The following two declarations are erroneous in that
    // since I don't provide a volatile declaration, the code
    // will typically hang after a few iterations.
    // static boolean req = false;
    // static boolean ack = false;    

    static int A;

    static int MaxSoFar = 0;

    public void run() {
  int temp;
  if (tid==0) {
      for (int i = 0; i < N; i++) {
    A = i;
    req = true;
    //System.out.println("REQ+!");
    while (ack==false) {
        // Thread.yield();
        //System.out.print(".");
    };
    //System.out.println("ACK+?");
    req = false;
    //System.out.println("REQ-!");
    while (ack==true) {
        // Thread.yield();
        //System.out.print(",");
    }
    //System.out.println("ACK-?");
      }
  }
  else {
      for (int i = 0; i < N; i++) {
    while (req==false) {
        // Thread.yield();
        //System.out.print("+");
    }
    //System.out.println("req+?");
    temp = A;
    if (temp > MaxSoFar) {
        MaxSoFar = temp;
        //System.out.println("Update to " + temp);
    }
    if (temp < MaxSoFar) {
        System.out.println("Found dip.");
    }
    ack = true;
    //System.out.println("ack+!");
    while (req==true) {
        // Thread.yield();
        //System.out.print("-");
    }
    //System.out.println("req-?");
    ack = false;
    //System.out.println("ack-!");
      }
  }
    }

    public static void main(String[] args) {

  try {
      N = Integer.parseInt(args[0]); // User-supplied N
  } // Parse user-supplied arg
  catch (Exception e) {
  } // Silently ignore exceptions.
  
  Thread t0 = new VGood(0);
  Thread t1 = new VGood(1);

  t0.start();
  t1.start();

  try {
      t0.join();
      t1.join();
  }
  catch (InterruptedException e) {
  }

  System.out.println("Done.");
    }
}
//--end

