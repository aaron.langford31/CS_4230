#include <stdio.h>
//#include <sys/sysinfo.h>
#include <pthread.h> 
#include <stdlib.h>
#include <math.h>

const long MAX_EXP = 32;
int Exp, Thres;

typedef struct {
  int L;
  int H;
} RNG;

// Global arrays over which DP is done
float *A;
float *B;
float *C;

void Usage(char *prog_name) {
   fprintf(stderr, "usage: %s <Exp>:int <Thres>:int\n", prog_name);
   fprintf(stderr, "Ensure that Thres <= pow(2, Exp)\n");
   exit(0);
}  /* Usage */

void Get_args(int argc, char **argv) {
   if (argc != 3) Usage(argv[0]);
   Exp = strtol(argv[1], NULL, 10);  
   if (Exp <= 0 || Exp > MAX_EXP) Usage(argv[0]);
   Thres = strtol(argv[2], NULL, 10);
   if (Thres < 1 || Thres >= (int) pow(2, Exp)) Usage(argv[0]);
}  

void serdp(RNG rng) {
  C[rng.H] = A[rng.H] * B[rng.H];
  //printf("C[%d]=%.2f=%.2f*%.2f\n", rng.H, C[rng.H], A[rng.H], B[rng.H]);
  for(int i=rng.H - 1; i>=rng.L; --i) {
    //printf("C[%d]=%.2f + (%.2f * %.2f)\n", i, C[i+1], A[i], B[i]);
    C[i] = C[i+1] + (A[i] * B[i]);
  }
}

void pdp(void* myrng) {
  RNG rng = *((RNG*)myrng);
  if ((rng.H - rng.L) <= Thres) {
    serdp(rng);
  }
  else {
    RNG rngL = rng;
    RNG rngH = rng;
    
    rngL.H = rng.L + (rng.H - rng.L)/2;
    rngH.L = rngL.H+1;
    
    pthread_t l, h;
    pthread_create(&l, NULL, (void*)&pdp, (void*)&rngL);
    pthread_create(&h, NULL, (void*)&pdp, (void*)&rngH);

    pthread_join(l, NULL);
    pthread_join(h, NULL);
  }
}

int get_nprocs_conf(void);
int get_nprocs(void);

int main(int argc, char **argv) {
  // Turn this on on Linux systems
  // On Mac, it does not work
  printf("This system has\
          %d processors configured and\
  	      %d processors available.\n",
          get_nprocs_conf(), get_nprocs());
  
  Get_args(argc, argv);  
  int Size = (int) pow(2, Exp);
  printf("Will do DP of %d sized arrays\n", Size);

  A = (float *) malloc(Size*sizeof(float));
  B = (float *) malloc(Size*sizeof(float));
  printf("Filling arrays now\n");
  srand(17); //Seed with 17
  for (int i=0; i<Size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }
  
  printf("A=[");
  for(int i=0; i<Size; i++) {
    printf("%.2f,", A[i]);
  }
  printf("]\nB=[");
  for(int i=0; i<Size; i++) {
    printf("%.2f,", B[i]);
  }
  printf("]\n");


  RNG rng;
  rng.L = 0;
  rng.H = Size-1;
  //printf("Serial dot product is %f\n", serdp(rng));
  
  printf("Now invoking parallel dot product\n");
  C = (float *) malloc(Size*sizeof(float));
  for(int i=0; i<Size; ++i) {
    C[i]=0;
  }
  
  pdp((void*)&rng);

  // reduce the final step
  for(int i=Size-Thres; i>0; i-=Thres){
    printf("C[%d] = %.2f\n", i, C[i]);
    C[i-Thres] += C[i];
  }

  printf("\n= %.2f\n", C[0]);

  free(A);
  free(B);
  free(C);
}
