#include <cmath>
#include <iostream>
#include <mutex>
#include <omp.h>
#include <stdlib.h>
#include <vector>

using namespace std;

template <class T>
class my_par_vec {
  vector<T> v;
  mutex mx;
public:
  void insert(T item) {
    mx.lock();
    v.push_back(item);
    mx.unlock();      
  }

  T get(int ix) {
    mx.lock();
    T ret = v[ix];
    mx.unlock();

    return ret;
  }

  int size() {
    mx.lock();
    int ret = v.size();
    mx.unlock();

    return ret;
  }
};

int main(int argc, char** argv) {
  if (argc < 3) { cout << "Bad args..." << endl; exit(-1); }

  int thread_count = strtol(argv[1], NULL, 10);
  int primes_range = strtol(argv[2], NULL, 10);

  cout << "thread_count: " << thread_count << endl;
  cout << "primes_range: " << primes_range << endl;

  my_par_vec<int> primes;

  #pragma omp parallel num_threads(thread_count)
  {
    #pragma omp for schedule(dynamic, 1000000)
    for (int i = 3; i < primes_range; i += 2) {
      int limit = (int) sqrt((float)i) + 1;
      bool is_prime = true;

      for (int j = 3; is_prime && j <= limit; j += 2) {
        if (i % j == 0) {
          is_prime = false;
        }
      }
      
      if (is_prime) {
        primes.insert(i);
      }
    }
  }

  cout << primes.get(0);
  for (int i = 1; i < primes.size(); i++) {
    cout << ", " << primes.get(i);
  }

  cout << endl;
}
