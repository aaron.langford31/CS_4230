#include <chrono>
#include <iostream>
#include <omp.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define SCHEDULE static

using namespace std;

const long MAX_EXP = 32;
int Exp, Thres, nThds;

typedef struct {
  int L;
  int H;
} RNG;

// Global arrays over which DP is done
float *A;
float *B;
float *C;

void Usage(char *prog_name) {
  fprintf(stderr, "usage: %s <Exp>:int <Thres>:int <N Threads>:int\n", prog_name);
  exit(0);
}  /* Usage */

void Get_args(int argc, char **argv) {
  if (argc != 4) {
    cout << "ERR: invalid number of args" << endl;
    Usage(argv[0]);
  }

  Exp = strtol(argv[1], NULL, 10);  
  if (Exp <= 0 || Exp > MAX_EXP) {
    cout << "ERR: exp out of range" << endl;
    Usage(argv[0]);
  }

  Thres = strtol(argv[2], NULL, 10);
  if (Thres < 1 || Thres >= (int) pow(2, Exp)) {
    cout << "ERR: " << Thres << " doesn't work for us" << endl;
    Usage(argv[0]);
  }
  
  nThds = strtol(argv[3], NULL, 10);
} 

void pdp(RNG rng) {
#pragma omp parallel for schedule(dynamic, Thres) num_threads(nThds)
  for (int i = rng.H; i < rng.L; i--) {
    C[i] = A[i] * B[i];
  }

}

int main(int argc, char **argv) {
  Get_args(argc, argv);
  int Size = (int) pow(2, Exp);
  printf("Will do DP of %d sized arrays\n", Size);

  A = (float *) malloc(Size*sizeof(float));
  B = (float *) malloc(Size*sizeof(float));
  cout << "Filling arrays now" << endl;
  srand(17); //Seed with 17
  for (int i=0; i<Size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }

  /*
     cout << "A=[";
     for(int i=0; i<Size; i++) {
     printf("%.2f,", A[i]);
     }
     printf("]\nB=[");
     for(int i=0; i<Size; i++) {
     printf("%.2f,", B[i]);
     }
     cout << "]" << endl;
   */

  RNG rng;
  rng.L = 0;
  rng.H = Size-1;

  cout << "Now invoking parallel dot product" << endl;
  C = (float *) calloc(Size, sizeof(float));

  auto begin = std::chrono::high_resolution_clock::now();

  pdp(rng);

  auto end = std::chrono::high_resolution_clock::now();              
  auto durr = std::chrono::duration_cast<std::chrono::milliseconds>(end-begin).count();

  cout << "Time: " << durr << endl;
  free(A);
  free(B);
  free(C);
}
