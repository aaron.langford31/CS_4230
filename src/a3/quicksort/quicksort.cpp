#include <iostream>
#include "tbb/tbb.h"
#include "tbb/parallel_reduce.h"
#include "tbb/blocked_range.h"

using namespace std;
using namespace tbb;

ptrdiff_t QUICKSORT_CUTOFF = 100;

typedef int T;

T* median_of_three(T* x, T* y, T* z) {
  return
    *x < *y ?
      *y < *z ?
        y     :
        *x < *z ?
          z     :
          x :
      *z < *y ?
        y     :
        *z < *x ?
          z     :
          x;
}

T* choose_partition_key(T* first, T* last) {
  size_t offset = (last-first) / 8;
  return median_of_three(
    median_of_three(first, first+offset, first+offset*2),
    median_of_three(first+offset*3, first+offset*4, first+offset*5),
    median_of_three(last-(2*offset+1), last-(offset+1), last-1)
  );
}

T* divide(T* first, T* last) {
  std::swap(*first, *choose_partition_key(first,last));
  T key = *first;
  T* middle = std::partition(first+1, last, [=](const T& x) { return x < key; } ) - 1;
  if ( middle != first ) {
    std::swap(*first, *middle);
  } else {
    if ( last == std::find_if(first+1, last, [=](const T& x) { return x < key; } ) )
     return NULL;
  }

  return middle;
}

void parallel_quicksort(T* first, T* last) {
  tbb::task_group g;
  while (last - first > QUICKSORT_CUTOFF ) {
    T* middle = divide(first, last);
    if (!middle) {
      g.wait();
      return;
    } 

    if (middle - first < last - (middle + 1)) {
      g.run([=](){ parallel_quicksort(first, middle); });
      first = middle + 1;
    } else {
      g.run([=](){ parallel_quicksort(middle + 1, last); });
      last = middle;
    }
  }

  std::sort(first, last);
  g.wait();
}

#define MAX_ASIZE 1000000000

int ASize;

void Get_args(int argc, char** argv);
void Usage(char* prog_name);

int* A;

int main(int argc, char** argv) {
  Get_args(argc, argv);
  A = (int*)malloc(ASize*sizeof(int));
  srand(17);

  int nthr = task_scheduler_init::default_num_threads();
  cout << "Will run up to " << nthr << " threads" << endl;

  for (int p = 1; p <= nthr; ++p) {
    for (int i = 0; i < ASize; i++) {
      A[i] = i;
    }
    
    tick_count t0 = tick_count::now();
    task_scheduler_init init(p);

    parallel_quicksort(&A[0], &A[ASize]);

    tick_count t1 = tick_count::now();
    double t = (t1-t0).seconds();

    cout << "ASize = " << ASize << ", nThr = " << p << ", T = " << t << endl;
  }
}

void Get_args(int argc, char** argv) {
  if (argc != 2) Usage(argv[0]);
  ASize = strtol(argv[1], NULL, 10);
  if (ASize <= 0 || ASize > MAX_ASIZE) Usage(argv[0]);
}

void Usage(char* prog_name) { 
  fprintf(stderr, "usage: %s ASize:int\n", prog_name);
  exit(0);
}
