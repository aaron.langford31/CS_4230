#include <chrono>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "tbb/tbb.h"

using namespace std;

const long MAX_EXP = 32;
int Exp, Thres, nThds;

typedef struct {
  int L;
  int H;
} RNG;

// Global arrays over which DP is done
float *A;
float *B;
float *C;

void Usage(char *prog_name) {
  fprintf(stderr, "usage: %s <Exp>:int <Thres>:int <N Threads>:int\n", prog_name);
  exit(0);
}  /* Usage */

void Get_args(int argc, char **argv) {
  if (argc != 4) {
    cout << "ERR: invalid number of args" << endl;
    Usage(argv[0]);
  }

  Exp = strtol(argv[1], NULL, 10);  
  if (Exp <= 0 || Exp > MAX_EXP) {
    cout << "ERR: exp out of range" << endl;
    Usage(argv[0]);
  }

  Thres = strtol(argv[2], NULL, 10);
  if (Thres < 1 || Thres >= (int) pow(2, Exp)) {
    cout << "ERR: " << Thres << " doesn't work for us" << endl;
    Usage(argv[0]);
  }
  
  nThds = strtol(argv[3], NULL, 10);
} 

double pdp(RNG rng) {
  tbb::parallel_for(rng.L, rng.H, Thres,
    [&](const size_t i) {
      C[i] = A[i] * B[i];
    }
  );
  return 0.0;
  /*return tbb::parallel_reduce(
    tbb::blocked_range<size_t>(rng.L, rng.H),
    0.0,
    [&](tbb::blocked_range<size_t> r, double sum) {
      for (size_t i = r.begin(); i < r.end(); i++) {
        sum += C[i];
      }
      return sum;
    },
    std::plus<double>()
  );*/
}

int main(int argc, char **argv) {
  Get_args(argc, argv);
  int Size = (int) pow(2, Exp);
  // printf("Will do DP of %d sized arrays\n", Size);

  A = (float *) malloc(Size*sizeof(float));
  B = (float *) malloc(Size*sizeof(float));
  // cout << "Filling arrays now" << endl;
  srand(17); //Seed with 17

  for (int i=0; i<Size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }

  RNG rng;
  rng.L = 0;
  rng.H = Size-1;

  // cout << "Now invoking parallel dot product" << endl;
  C = (float *) calloc(Size, sizeof(float));

  auto begin = std::chrono::high_resolution_clock::now();

  double res = pdp(rng);

  auto end = std::chrono::high_resolution_clock::now();              
  auto durr = std::chrono::duration_cast<std::chrono::milliseconds>(end-begin).count();

  cout << Size << "," << Thres << "," << nThds << "," << durr << "," << res << endl;
  free(A);
  free(B);
  free(C);
}
