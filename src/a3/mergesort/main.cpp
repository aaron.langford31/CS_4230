#include <chrono>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "tbb/tbb.h"
#include "tbb/task_scheduler_init.h"

#include "merge_tbb.h"
#include "merge_sort_tbb.h"

using namespace std;

const long MAX_EXP = 32;
int Exp, nThds;

void Usage(char *prog_name) {
  fprintf(stderr, "usage: %s <Exp>:int <N Threads>:int\n", prog_name);
  exit(0);
}  /* Usage */

void Get_args(int argc, char **argv) {
  if (argc != 3) {
    cout << "ERR: invalid number of args" << endl;
    Usage(argv[0]);
  }

  Exp = strtol(argv[1], NULL, 10);  
  if (Exp <= 0 || Exp > MAX_EXP) {
    cout << "ERR: exp out of range" << endl;
    Usage(argv[0]);
  }
  
  nThds = strtol(argv[2], NULL, 10);
} 

int main(int argc, char **argv) {
  Get_args(argc, argv);
  int Size = (int) pow(2, Exp);

  cout << "Filling array with random numbers" << endl;
  vector<float>* arr = new vector<float>();
  srand(17); //Seed with 17

  for (int i=0; i<Size; ++i) {
    arr->push_back( rand() % 16 );
  }
  

  cout << "Beginning time experiments" << endl;
 
  tbb::task_scheduler_init init(nThds);
  auto begin = std::chrono::high_resolution_clock::now();

  call_parallel_merge_sort(arr->begin(), arr->end());

  auto end = std::chrono::high_resolution_clock::now();              
  auto durr = std::chrono::duration_cast<std::chrono::milliseconds>(end-begin).count();

  cout << Size << "," << nThds << "," << durr << endl;
}
