typedef std::vector<float>::iterator it_t;

// merge sequences [xs,xe) and [ys,ye) to output [zs,(xe-xs)+(ye-ys)
void parallel_merge(it_t xs, it_t xe, it_t ys, it_t ye, it_t zs ) {
    const size_t MERGE_CUT_OFF = 2000;
    if( xe-xs + ye-ys <= MERGE_CUT_OFF ) {
        std::merge(xs,xe,ys,ye,zs);
    } else {
        it_t xm, ym;
        if( xe-xs < ye-ys  ) {
            ym = ys + (ye-ys) / 2;
            xm = std::upper_bound(xs,xe,*ym);
        } else {
            xm = xs+(xe-xs)/2;
            ym = std::lower_bound(ys,ye,*xm);
        }
        it_t zm = zs + (xm-xs) + (ym-ys);
        tbb::parallel_invoke( [=]{parallel_merge( xs, xm, ys, ym, zs );},
                              [=]{parallel_merge( xm, xe, ym, ye, zm );} );
    }
}
